import numpy as np

class BMWSSS_solution():

    def __init__(self, solution, init_buffer, max_buffer, target_volume, tolerance):
        self.n_days = len(solution)//2 
        self.solution = np.transpose(np.array([solution[:self.n_days], solution[self.n_days:]]))
        self.init_buffer = init_buffer
        self.max_buffer = max_buffer
        self.target_volume = target_volume
        self.tolerance = tolerance

    def daily_buffer_amount(self):
        dim = np.shape(self.solution)
        buffer_list = np.zeros(dim[0]+1)
        buffer_list[0] = self.init_buffer
        for i in range(1,self.n_days+1):
            buffer_list[i] = max(0, buffer_list[i-1] + self.solution[i-1,0] - self.solution[i-1,1])
        return buffer_list

    def cumulative_volume(self):
        volume = np.zeros(self.n_days)
        buffer_list = self.daily_buffer_amount()
        for i in range(self.n_days):
            volume[i] = np.sum(self.solution[:i+1,0]) + self.init_buffer - buffer_list[i+1]
        return volume

    def isFeasible(self):
        buffer_list = self.daily_buffer_amount()
        volume = self.cumulative_volume()
        buffer_violations = np.where(buffer_list > self.max_buffer)
        if len(buffer_violations) == 0 or volume[-1] < self.target_volume - self.tolerance or volume[-1] > self.target_volume + self.tolerance:
            return False
        else:
            return True

class BMWGen_solution():

    def __init__(self, solution, init_buffer, max_buffer, target_volume, tolerance, n_shop_types, n_shops, n_shifts, units_per_hour, calendar, block_len, shop_shifts, shop_costs, penalty_scheme):
        self.n_days = len(solution)//(sum(n_shops)*n_shifts)
        self.solution = np.transpose(np.reshape(solution, (sum(n_shops), n_shifts*self.n_days)))
        self.init_buffer = init_buffer
        self.max_buffer = max_buffer
        self.target_volume = target_volume
        self.tolerance = tolerance
        self.n_shop_types = n_shop_types
        self.n_shops = n_shops
        self.n_shifts = n_shifts
        self.units_per_hour = units_per_hour
        self.calendar = calendar
        self.block_len = block_len
        self.shop_shifts = shop_shifts
        self.model_ratios = [i/sum(self.target_volume) for i in self.target_volume]
        self.shop_costs = shop_costs
        self.penalty_scheme = penalty_scheme
        
    def daily_buffer_amount(self):
        buffer_list = np.zeros((self.n_days*self.n_shifts + 1, self.n_shops[0]*(self.n_shop_types - 1)))
        buffer_list[0] = self.init_buffer
        solution_cal = np.multiply(self.solution, self.calendar)
        # Handles maximum of two buffers. We have two cases: one buffer and two buffers.
        if self.n_shop_types == 3:
            for d in range(self.n_days*self.n_shifts):
                # Loops over shifts
                for j in range((self.n_shops[0])):
                    # Loops over sub-buffers
                    # First buffer computations
                    buffer_list[d+1, j] = max(0, buffer_list[d,j] + solution_cal[d,j]*self.units_per_hour[j] - self.model_ratios[j]*solution_cal[d,-2]*self.units_per_hour[-2])
                    # Second buffer computations
                    if buffer_list[d+1, j] == 0:
                        buffer_list[d+1, j+self.n_shops[0]] = max(0, buffer_list[d,j+self.n_shops[0]] + buffer_list[d,j] + solution_cal[d,j]*self.units_per_hour[j] - self.model_ratios[j]*solution_cal[d,-1]*self.units_per_hour[-1])
                    else:
                        buffer_list[d+1, j+self.n_shops[0]] = max(0, buffer_list[d,j+self.n_shops[0]] + self.model_ratios[j]*solution_cal[d,-2]*self.units_per_hour[-2] - self.model_ratios[j]*solution_cal[d,-1]*self.units_per_hour[-1])
        else:
            for d in range(self.n_days*self.n_shifts):
                # Loops over shifts
                for j in range(self.n_shops[0]):
                    # Loops over sub-buffers
                    buffer_list[d+1, j] = max(0, buffer_list[d,j] + solution_cal[d,j]*self.units_per_hour[j] - self.model_ratios[j]*solution_cal[d,-1]*self.units_per_hour[-1])

        return buffer_list

    def cumulative_volume(self):
        volume = np.zeros((self.n_days+1, self.n_shops[0]))
        solution_cal = np.multiply(self.solution, self.calendar)
        buffer_list = self.daily_buffer_amount()
        if self.n_shop_types == 3:
            for d in range(1,self.n_days+1):
                for j in range(self.n_shops[0]):
                    volume[d,j] = volume[d-1,j] + solution_cal[d-1,j]*self.units_per_hour[j] + buffer_list[d-1,j] + buffer_list[d-1,j+self.n_shops[0]] - buffer_list[d,j] - buffer_list[d,j+self.n_shops[0]]
        else:
            for d in range(1,self.n_days+1):
                for j in range(0,self.n_shops[0]):
                    volume[d,j] = volume[d-1,j] + solution_cal[d-1,j]*self.units_per_hour[j] + buffer_list[d-1,j] - buffer_list[d,j]
        return volume
    
    def isFeasible(self):
        """
        Determines the feasibility of the configuration by checking for violations in buffer and volume.
        """ 
        