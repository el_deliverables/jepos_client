from typing import Union
from pydantic import Field

from ..enums import *
from ..validator_tools import *
from ..baseModel import BaseModel
from .betaParams import BetaParams, DefaultBetaParams
from .acceptanceParams import AcceptanceParams, DefaultAcceptanceParams

class SACorrelationParams(BaseModel):
    """
    A SACorrelationParams object determine the Simulated Annealing correlation routine.

    Attributes
    ----------
    correlation_type: str, constant CorrelationType.simulated_annealing
        value indicating the Simulated Annealing correlation routine
    correlation_steps: int
        the number of times that the correlate() procedure is executed
        correlate() adjusts the beta schedule
    beta_params: BetaParams
        the beta parameters
    """
    correlation_type: CorrelationType = Field(CorrelationType.simulated_annealing, const=True)
    correlation_steps: int = 100
    beta_params: BetaParams = DefaultBetaParams()

    _ = check_positive('correlation_steps')

class PTCorrelationParams(BaseModel):
    """
    A PTCorrelationParams object determine the Parallel Tempering correlation routine.

    Attributes
    ----------
    correlation_type: str, constant CorrelationType.parallel_tempering
        value indicating the Parallel Tempering correlation routine
    correlation_steps: int
        the number of times that the correlate() procedure is executed
        correlate() performs beta swaps and equilibration
    beta_params: BetaParams
        the beta parameters
    acceptance_params: AcceptanceParams
        the acceptance parameters
    """
    correlation_type: CorrelationType = Field(CorrelationType.parallel_tempering, const=True)
    correlation_steps: int = 100
    beta_params: BetaParams = DefaultBetaParams()
    acceptance_params: AcceptanceParams = DefaultAcceptanceParams()

    _ = check_positive('correlation_steps')

class PACorrelationParams(BaseModel):
    """
    A PACorrelationParams object determine the Population Annealing correlation routine.

    Attributes
    ----------
    correlation_type: str, constant CorrelationType.population_annealing
        value indicating the Population Annealing correlation routine
    correlation_steps: int
        the number of times that the correlate() procedure is executed
        correlate() adjusts the beta schedule and performs beta swaps and equilibration
    beta_params: BetaParams
        the beta parameters
    """
    correlation_type: CorrelationType = Field(CorrelationType.population_annealing, const=True)
    correlation_steps: int = 100
    beta_params: BetaParams = DefaultBetaParams()

    _ = check_positive('correlation_steps')

class RSCorrelationParams(BaseModel):
    """
    A RSCorrelationParams object determine the Random Sampling correlation routine.

    Attributes
    ----------
    correlation_type: str, constant CorrelationType.random_sampling
        value indicating the Random Sampling correlation routine
    correlation_steps: int
        the number of times that the correlate() procedure is executed
    """
    correlation_type: CorrelationType = Field(CorrelationType.random_sampling, const=True)
    correlation_steps: int = 100

    _ = check_positive('correlation_steps')

CorrelationParams = Union[SACorrelationParams, PTCorrelationParams, PACorrelationParams, RSCorrelationParams]
DefaultCorrelationParams = SACorrelationParams