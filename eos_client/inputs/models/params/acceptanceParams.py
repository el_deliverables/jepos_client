from typing import List, Union
from pydantic import Field, root_validator

from ..enums import *
from ..validator_tools import *
from ..baseModel import BaseModel

class MetropolisAcceptanceParams(BaseModel):
    """
    A MetropolisAcceptanceParams object specifies the metropolis acceptance rule
    for the Monte-Carlo updates and the beta swaps in Parallel Tempering.

    Attributes
    ----------
    acceptance_type: str, constant AcceptanceType.metropolis
        value indicating a metropolis acceptance rule
    """
    acceptance_type: AcceptanceType = Field(AcceptanceType.metropolis, const=True)

class GlauberAcceptanceParams(BaseModel):
    """
    A GlauberAcceptanceParams object specifies the glauber acceptance rule
    for the Monte-Carlo updates and the beta swaps in Parallel Tempering.

    Attributes
    ----------
    acceptance_type: str, constant AcceptanceType.glauber
        value indicating a glauber acceptance rule
    """
    acceptance_type: AcceptanceType = Field(AcceptanceType.glauber, const=True)

class SymMetropolisAcceptanceParams(BaseModel):
    """
    A SymMetropolisAcceptanceParams object specifies the symmetric metropolis acceptance rule
    for the Monte-Carlo updates and the beta swaps in Parallel Tempering.

    Attributes
    ----------
    acceptance_type: str, constant AcceptanceType.sym_metropolis
        value indicating a symmetric metropolis acceptance rule
    offset: float
    """
    acceptance_type: AcceptanceType = Field(AcceptanceType.sym_metropolis, const=True)
    offset: float = 0.1

    _ = check_positive('offset') # TODO: not sure need to be positive ...


AcceptanceParams = Union[MetropolisAcceptanceParams, GlauberAcceptanceParams, SymMetropolisAcceptanceParams]
DefaultAcceptanceParams = MetropolisAcceptanceParams