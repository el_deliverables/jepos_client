from typing import List, Union
from pydantic import Field, validator, root_validator

from ..enums import *
from ..baseModel import BaseModel
from ..validator_tools import check_non_empty, check_positive

class RangeVar(BaseModel):
    min: float = 0.0
    max: float = 1.0

    @root_validator(skip_on_failure=True)
    def check_min_max(cls, values):
        min, max = values.get('min'), values.get('max')
        if min > max: # TODO? is it okay to have same max and min ? > or >=
            raise ValueError('min has to be lower than max')
        return values

# TODO? is only binary encoding allowed ?
# TODO what or if default var for range_of_vars ?...
# TODO len(range_of_vars) == n in problem, or == 1

class QUBOCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.QUBO, const=True)
    encoding: List[float] = [-1., 1.]
    nr_of_vars: int = 5
    terms: List[List[int]] = [[1,2],[2,3],[3,4],[4,5],[5,1]]
    weights: List[float] = [3.1, 2.3, 4.1, 3.3, 2.8]

    # _ = check_positive('nr_of_vars')
    # _ = check_positive('terms', opt={'each_item':True})

    # @root_validator(skip_on_failure=True)
    # def check_weights_below_bin_capacity(cls, values):
    #     bin_capacity = values["bin_capacity"]
    #     item_weights = values["item_weights"]
    #     if max(item_weights) > bin_capacity:
    #         raise ValueError('Some of the item weights provided are larger than the bin capacity: infeasible instance.')
    #     return values

    #     _ = check_non_empty('city_coords')

    # @validator('city_coords')
    # def check_len_city_coord(cls, v):
    #     ref_len = len(v[0])
    #     for i in range(len(v)):
    #         if len(v[i]) != ref_len:
    #             dim = list(map(lambda i: len(i), v))
    #             raise ValueError(f'all city coordinates should have same dimension, given dimensions are {dim}')

    #     return v





class PUBOCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.PUBO, const=True)
    encoding: List[float] = [-1.0, 1.0]

class QUCOCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.QUCO, const=True)
    range_of_vars: List[RangeVar] = [RangeVar()]

class PUCOCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.PUCO, const=True)
    range_of_vars: List[RangeVar] = [RangeVar()]

class TSPCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.discrete_TSP, const=True)
    city_coords: List[List[float]]

    _ = check_non_empty('city_coords')

    @validator('city_coords')
    def check_len_city_coord(cls, v):
        ref_len = len(v[0])
        for i in range(len(v)):
            if len(v[i]) != ref_len:
                dim = list(map(lambda i: len(i), v))
                raise ValueError(f'all city coordinates should have same dimension, given dimensions are {dim}')

        return v

class NPCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.NP, const=True)
    np_set: List[float] = [1,2,3,4,5]

    _ = check_positive('np_set', opt={'each_item':True})

class RandomPUBOCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.random_PUBO, const=True)
    encoding: List[float] = [-1.0, 1.0]

class RandomPUCOCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.random_PUCO, const=True)
    range_of_vars: List[RangeVar] = [RangeVar()]

class RandomTSPCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.random_TSP, const=True)
    nr_of_cities: int

    _ = check_positive('nr_of_cities')

class RandomNPCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.random_NP, const=True)
    nr_of_vars: int

    _ = check_positive('nr_of_vars')

class SinusoidLadderCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.sinusoid_ladder, const=True)
    nr_of_vars: int
    range_of_vars: List[RangeVar] = [RangeVar()]
    grad: float

    _ = check_positive('grad')

    # TODO len(range_of_vars) == nr_of_vars or == 1

class BPCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.BP, const=True)
    bin_capacity: float
    item_weights: List[float]

    _ = check_positive('bin_capacity')
    _ = check_positive('item_weights', opt={'each_item':True})

    @root_validator(skip_on_failure=True)
    def check_weights_below_bin_capacity(cls, values):
        bin_capacity = values["bin_capacity"]
        item_weights = values["item_weights"]
        if max(item_weights) > bin_capacity:
            raise ValueError('Some of the item weights provided are larger than the bin capacity: infeasible instance.')
        return values


class RandomBPCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.random_BP, const=True)
    bin_capacity: float
    nr_of_items: int
    weight_type: str # weight_type specifies the type of the item weigths (e.g 'Int64')

    _ = check_positive('bin_capacity')
    _ = check_positive('nr_of_items')
    _ = check_non_empty("weight_type")

class BMWSSSCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.BMW_SSS, const=True)
    n_days: int = 10
    delta: int = 2
    init_buffer: int = 5
    max_buffer: int = 20
    allowed_shifts: List[List[float]] = [[0.0,5.0,8.0,10.0],[0.0,4.0,7.0,9.0]]
    target_vol: int = 70
    buffer_penalty: float = 5.0
    vol_penalty: float = 5.0
    
    _ = check_positive('n_days')
    _ = check_positive('delta')
    _ = check_positive('init_buffer')
    _ = check_positive('max_buffer')
    _ = check_positive('target_vol')

    @root_validator(skip_on_failure=True)
    def check_target_vol_more_delta(cls, values):
        target_vol = values["target_vol"]
        delta = values["delta"]
        if delta > target_vol:
            raise ValueError('Delta provided is larger than the target volume: infeasible instance.')
        return values

class BMWGenCostfuncParams(BaseModel):
    cost_type: CostType = Field(CostType.BMW_Gen, const=True)
    n_blocks: int
    block_len: int
    init_buffer: List[List[float]]
    max_buffer: List[float]
    target_vol: List[float]
    tolerance: List[float]
    n_diff_shops: int
    n_body_shops: int
    n_shifts: int 
    shop_shifts: List[List[List[float]]]
    units_per_hour: List[float]
    shop_costs: List[float]
    shift_change_costs: List[float]
    model_ratios: List[float]
    penalty_coeff: float
    penalty_scheme: int
    calendar: List[List[List[int]]]

    _ = check_positive('n_blocks')
    _ = check_positive('block_len')
    _ = check_positive('n_diff_shops')
    _ = check_positive('n_body_shops')
    _ = check_positive('n_shifts')
    _ = check_positive('penalty_coeff')
    _ = check_positive('penalty_scheme')
    
    @root_validator(skip_on_failure=True)
    def check_calendar(cls, values):
        n_shifts = values["n_shifts"]
        n_blocks = values["n_blocks"]
        block_len = values["block_len"]
        calendar = values["calendar"]
        n_diff_shops = values["n_diff_shops"]
        n_body_shops = values["n_body_shops"]
        n_shops = n_diff_shops + n_body_shops - 1
        n_days = n_blocks * block_len

        if n_days != len(calendar):
            raise ValueError("Size of the calendar is inconsistent with the number of days.")

        # Check calendar values are binary
        for i in calendar:
            if n_shifts != len(i):
                raise ValueError("Size of the calendar is inconsistent with the number of shifts per day.")
            for j in i:
                if n_shops != len(j):
                    raise ValueError("Size of the calendar is inconsistent with the number of shops.")
                for k in j:
                    if k != 0 and k != 1:
                        raise ValueError("Every value in the calendar should be binary.")
        
        return values
    
    @root_validator(skip_on_failure=True)
    def check_init_buffer(cls, values):
        init_buffer = values["init_buffer"]
        n_diff_shops = values["n_diff_shops"]
        n_body_shops = values["n_body_shops"]

        if len(init_buffer) != n_body_shops:
                raise ValueError("Number of buffers does not match the number of body shops.")

        for i in init_buffer:
            if len(i) != (n_diff_shops - 1):
                raise ValueError("Number of buffers does not match the number of types of shops.")
            for j in i:
                if j <= 0:
                    raise ValueError("Initial buffer values should be positive.")
        
        return values
    
    @root_validator(skip_on_failure=True)
    def check_max_buffer(cls, values):
        max_buffer = values["max_buffer"]
        n_diff_shops = values["n_diff_shops"]
        for i in max_buffer:
            if i <= 0:
                raise ValueError("Max buffer values should be positive.")
        
        if len(max_buffer) != (n_diff_shops - 1):
            raise ValueError("Number of buffers does not match the number of types of shops.")
        
        return values
    
    @root_validator(skip_on_failure=True)
    def check_target_volume(cls, values):
        target_vol = values["target_vol"]
        n_body_shops = values["n_body_shops"]
        for i in target_vol:
            if i <= 0:
                raise ValueError("Target volume values should be positive.")
        
        if len(target_vol) != n_body_shops:
            raise ValueError("Target volume does not match the number of body shops.")
        
        return values
    
    @root_validator(skip_on_failure=True)
    def check_tolerance(cls, values):
        tolerance = values["tolerance"]
        n_body_shops = values["n_body_shops"]
        for i in tolerance:
            if i <= 0:
                raise ValueError("Tolerance values should be positive.")
                
        if len(tolerance) != n_body_shops:
            raise ValueError("Tolerance does not match the number of body shops.")
        
        return values
    
    @root_validator(skip_on_failure=True)
    def check_shop_shifts(cls, values):
        shop_shifts = values["shop_shifts"]
        n_body_shops = values["n_body_shops"]
        n_diff_shops = values["n_diff_shops"]
        n_shifts = values["n_shifts"]

        if len(shop_shifts) != (n_body_shops + n_diff_shops - 1):
            raise ValueError("Shifts do not match the number of shops.")

        for i in shop_shifts:
            if len(i) != n_shifts:
                raise ValueError("Shifts do not match the number of shifts per day.")
            for j in i:
                for k in j:
                    if k < 0:
                        raise ValueError("Shift hour lengths should not be negative.")

        return values

    @root_validator(skip_on_failure=True)
    def check_units_per_hour(cls, values):
        units_per_hour = values["units_per_hour"]
        shop_shifts = values["shop_shifts"]
        for i in units_per_hour:
            if i <= 0:
                raise ValueError('Production capacity of a factory should be a positive value.')
        
        if len(units_per_hour) != len(shop_shifts):
            raise ValueError("Production values does not match the number of shops.")

        return values

    @root_validator(skip_on_failure=True)
    def check_shop_costs(cls, values):
        shop_costs = values["shop_costs"]
        shop_shifts = values["shop_shifts"]
        for i in shop_costs:
            if i <= 0:
                raise ValueError("Cost of operating a shop should be a positive value.")

        if len(shop_costs) != len(shop_shifts):
            raise ValueError("Production cost values does not match the number of shops.")

        return values
    
    @root_validator(skip_on_failure=True)
    def check_shift_change_costs(cls, values):
        shift_change_costs = values["shift_change_costs"]
        shop_shifts = values["shift_change_costs"]
        for i in shift_change_costs:
            if i < 0:
                raise ValueError("Cost of changing shifts should not be negative.")
                
        if len(shift_change_costs) != len(shop_shifts):
            raise ValueError("Number of shift change costs does not match the number of shops.")
        
        return values
    
    @root_validator(skip_on_failure=True)
    def check_model_ratios(cls, values):
        model_ratios = values["model_ratios"]
        n_body_shops = values["n_body_shops"]
        for i in model_ratios:
            if i < 0 or i > 1:
                raise ValueError("Ratio should be between 0 and 1.")
                
        if sum(model_ratios) != 1:
            raise ValueError("Sum of the ratio should be equal to 1.")
            
        if len(model_ratios) != n_body_shops:
            raise ValueError("Ratio list does not match the number of body shops.")
            
        return values

CostfuncParams = Union[QUBOCostfuncParams, PUBOCostfuncParams, QUCOCostfuncParams, PUCOCostfuncParams,
                        TSPCostfuncParams, NPCostfuncParams, SinusoidLadderCostfuncParams, BPCostfuncParams,
                        RandomPUBOCostfuncParams, RandomPUCOCostfuncParams, RandomTSPCostfuncParams,
                        RandomNPCostfuncParams, RandomBPCostfuncParams, BMWSSSCostfuncParams, BMWGenCostfuncParams]
DefaultCostfuncParams = QUBOCostfuncParams
