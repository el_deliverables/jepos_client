from pydantic import StrictStr, root_validator

from .enums import *
from .baseModel import BaseModel
from .validator_tools import *
from .compatiblity_tools import *

from .problem import Problem
from .optimiser import Optimiser

class ConsistencyError(ValueError):
    pass

class Input(BaseModel):
    """
    Class to group all the input parameters to be given to the jepos solver.

    Note
    ----
    Since this class is to construct a jepos input, the attribute's significations
    are the same as in the jepos input.

    Attributes
    ----------
    user: str
        the user name
    name: str
        a name that the user can give to the problem statement
    problem: Problem
        the problem to solve
    optimiser: Optimiser
        the optimiser and it's parameters to use to solve the problem
    """
    user: StrictStr = 'client'
    name: StrictStr = 'random_name'
    problem: Problem
    optimiser: Optimiser = Optimiser()

    ## Validations

    _ = check_non_empty('user', 'name')

    @root_validator(skip_on_failure=True)
    def check_compatibility(cls, values):
        """
        Verifies that the specified parameter types are compatible with each other.

        Raises
        ------
        ConsistencyError
            If an inconsistency in the parameters is found.
        """
        # get vector of param types
        config_param_types = get_config_param_types(values, ['root'])

        # search for inconsistencies
        inconsistencies, conflict_message = get_inconsistencies(config_param_types)

        if len(inconsistencies): # at least one conflict found
            raise ConsistencyError(conflict_message)

        return values

    ## Methods

    def export_json(self, filename):
        """
        Method to export this input into the specified file in a json format.

        Parameters
        ----------
        filename: str
            the name of the file in which to save the json serialization of this input

        Returns
        -------
        str
            the json serialization of this input
        """
        input_json = self.json()
        with open(filename, '+w') as outfile:
            outfile.write(input_json)
        return input_json
