from typing import List, Union
from pydantic import Field

from ..enums import *
from ..baseModel import BaseModel

class RandomInitParams(BaseModel):
    """
    A RandomInitParams object specifies to randomly initialize the ensemble of replicas.

    Attributes
    ----------
    init_type: str, constant InitType.random
        value indicating to randomly initialize the ensemble
    """
    init_type: InitType = Field(InitType.random, const=True)

class CustomInitParams(BaseModel):
    """
    A CustomInitParams object determines how to initialize the initial ensemble of replicas.

    Attributes
    ----------
    init_type: str, constant InitType.custom
        value indicating to initialize the ensemble with the given replicas
    replicas: list of list of float
        the replicas with which to initialize the ensemble
        If the first dimension of "replicas" is not the same as "nr_of_replicas",
        then "replicas" is repeated until all "nr_of_replicas" are filled.
    """
    init_type: InitType = Field(InitType.custom, const=True)
    replicas: List[List[float]]

class TSPRandomInitParams(BaseModel):
    """
    A TSPRandomInitParams object specifies to randomly initialize the ensemble of replicas
    for a TSP problem.

    Attributes
    ----------
    init_type: str, constant InitType.tsp_random
        value indicating to randomly initialize the ensemble
    """
    init_type: InitType = Field(InitType.tsp_random, const=True)

InitParams = Union[RandomInitParams, CustomInitParams, TSPRandomInitParams]
DefaultInitParams = RandomInitParams