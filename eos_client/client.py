import requests
from urllib.error import HTTPError

CLIENT_VERSION = '0.0.1'

class Client:
    def __init__(self, username="entropica", password="entropica", token="123456", eos_version="dev"):
        self.username = username
        self.password = password  # Not sure if the password should be plainly stored here ...
        self.grant_type = ''
        self.scope = ''
        self.client_id = ''
        self.client_secret = ''
        self.token = token
        self.eos_version = eos_version
        self.client_version = CLIENT_VERSION

        # Select correct backend
        urls = {}

        if self.eos_version == 'dev':
            port = '8586'
        elif self.eos_version == 'stable':
            port = '8585'
        else:
            raise Exception(
                f'EOS version \'{self.eos_version}\' is not allowed, only \'dev\' and \'stable\' are currently supported'
            )

        # print(
        #     f'Note you are currently using EOS version {self.eos_version} '
        # )

        urls['url_optimizer'] = f'http://localhost:{port}/api/v1/utils/run_optimiser/'
        urls['url_token'] = f'http://localhost:{port}/api/v1/login/access-token/'
        urls['url_get_job_by_id'] = f'http://localhost:{port}/api/v1/result/'
        urls['url_get_job_status'] = f'http://localhost:{port}/api/v1/jobs/jobs'
        self.urls = urls

    def authenticate(self):
        """
        Authenticate the user. Sets the tocken to the correct bearer
        """

        headers = {
            'content-type': 'application/x-www-form-urlencoded',
            'accept': 'application/json'
        }

        # Leo: This try / except catches HTTP errors (such as 404, I believe)
        try:
            resp_auth = requests.post(self.urls['url_token'],
                                      data=vars(self),
                                      headers=headers)
        except HTTPError as e:
            # Get your code from the exception object like this
            print(e.response.status_code)
            # Or you can get the code which will be available from r.status_code
            print(resp_auth.status_code)


        # Leo: This try / except catches statues other than 200/2001
        try:
            resp_auth.raise_for_status()
        except requests.exceptions.HTTPError as e:
            print("Error: " + str(e))
            print("Response " + str(resp_auth.json()))
            return

        token = resp_auth.json()['token_type'] + " " + resp_auth.json(
        )['access_token']
        self.token = {'Authorization': token}

        print('Authorized')
        
    @staticmethod    
    def get_credentials(user):
        if user == 'cdl_demo': 
            credential = "demo@entropicalabs.com", "entropica"
        else:
            print('No pre-lodaed credentials. Setting them to None')
            credential = None
        return credential
