from typing import Union
from pydantic import Field

from ..enums import *
from ..validator_tools import *
from ..baseModel import BaseModel
from .acceptanceParams import AcceptanceParams, DefaultAcceptanceParams

class DiscreteMcupdateParams(BaseModel):
    """
    A DiscreteMcupdateParams object specifies a discrete Monte Carlo update rule.

    Attributes
    ----------
    mcupdate_type: str, constant McupdateType.discrete
        value indicating a discrete Monte Carlo update rule
    n_changes: int
        the number of spins flipped during a single Monte-Carlo update
    acceptance_params: AcceptanceParams
        the acceptance rule
    """
    mcupdate_type: McupdateType = Field(McupdateType.discrete, const=True)
    n_changes: int = 1
    acceptance_params: AcceptanceParams = DefaultAcceptanceParams()

class ContinuousMcupdateParams(BaseModel):
    """
    A ContinuousMcupdateParams object specifies a continuous Monte Carlo update rule.

    Attributes
    ----------
    mcupdate_type: str, constant McupdateType.continuous
        value indicating a continuous Monte Carlo update rule
    step_nr: int
        the size of the average step by (range.max - range.min) / step_nr
    n_changes: int
        the number of spins flipped during a single Monte-Carlo update
    acceptance_params: AcceptanceParams
        the acceptance rule
    """
    mcupdate_type: McupdateType = Field(McupdateType.continuous, const=True)
    step_nr: int = 1 # TODO probably not good default value
    n_changes: int = 1
    acceptance_params: AcceptanceParams = DefaultAcceptanceParams()

class DiscreteCitySwapMcupdateParams(BaseModel):
    """
    A DiscreteCitySwapMcupdateParams object specifies a discrete city swap Monte-Carlo
    update for travelling salesman problems.

    Attributes
    ----------
    mcupdate_type: str, constant McupdateType.discrete_city_swap
        value indicating a discrete city swap Monte Carlo update rule
    acceptance_params: AcceptanceParams
        the acceptance rule
    """
    mcupdate_type: McupdateType = Field(McupdateType.discrete_city_swap, const=True)
    acceptance_params: AcceptanceParams = DefaultAcceptanceParams()

class BinaryCitySwapMcupdateParams(BaseModel):
    """
    A BinaryCitySwapMcupdateParams object specifies a binary city swap Monte-Carlo
    update for travelling salesman problems.

    Attributes
    ----------
    mcupdate_type: str, constant McupdateType.binary_city_swap
        value indicating a binary city swap Monte Carlo update rule
    acceptance_params: AcceptanceParams
        the acceptance rule
    """
    mcupdate_type: McupdateType = Field(McupdateType.binary_city_swap, const=True)
    acceptance_params: AcceptanceParams = DefaultAcceptanceParams()

McupdateParams = Union[DiscreteMcupdateParams, ContinuousMcupdateParams, DiscreteCitySwapMcupdateParams, BinaryCitySwapMcupdateParams]
DefaultMcupdateParams = DiscreteMcupdateParams