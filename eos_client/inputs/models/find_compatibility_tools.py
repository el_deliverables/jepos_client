'''
This file contains functions to find a consistent configuration.
'''

from .compatiblity_tools import get_inconsistencies
from .constants import *

## Find consistent param type values given already setted param_types

def is_consistent(config_params_types):
    '''
    Whether the given list of param types in consistent.

    :config_param_types: list of param type values (in the current configuration)
    '''
    inconsistencies, _ = get_inconsistencies(config_params_types)

    return len(inconsistencies) == 0

def get_all_paths(node, adj_list):
    '''
    Recursively computes all the paths in the graph defined by its adjacency list adj_list.

    N.B. there should be no cycle in the given graph or the recursive calls never end.

    :node:      str, the current node in the graph
    :adj_list:  dict, the adjacency list of the graph
    '''
    if node in adj_list:
        adj_imp = adj_list[node]
        return [[node] + branch for imp in adj_imp for branch in get_all_paths(imp, adj_list)]
    return [[node]]

def implemented_values(node, setted_values):
    '''
    Returns all implemented values for the given param type node, if the value of this param type
    is not already fixed according to setted_values.
    If the given node is not a param type (but already an implemented param type value), return it.

    :node: str, name of a param type or a param type value
    :setted_values: list of param type values that are fixed
    '''
    if node in IMP: # node is a param type
        instances = IMP[node]

        # if implementation is setted, return only the setted value
        setted_values = set(setted_values)
        intersection = setted_values.intersection(instances)
        if len(intersection):
            return list(intersection)

        # otherwise, return all implemented values for the given param type
        return instances

    return [node]

def get_edge_adjacency(edge, setted_values):
    '''
    Construct the adjacency list corresponding to the given edge

    :edge:          tuple, pair of adjacent param types
    :setted_values: list of param type values that are fixed
    '''
    adj = {}
    node, adj_node = edge
    for i in implemented_values(node, setted_values):
        if (not i in EDGE_RULE.keys()) or (i == node):
            adj.update({i : implemented_values(adj_node, setted_values)})
    return adj

def get_adjacency_list(setted_values):
    '''
    Constructs the adjacency list of the graph of param type configurations.

    EDGE_RULE describes the edges between param types. Each param type is replaced by the list
    of implemented values described in IMP.

    :setted_values: list of param type values that are fixed
    '''
    adj_list = {}
    for edge in EDGE_RULE.items():
        adj_list.update(get_edge_adjacency(edge, setted_values))
    return adj_list

def find_cons_config(setted_values):
    '''
    Given a list of setted param-type values, finds a consistent configuration around it.

    It first constructs the adjacency list that describes the graph of all possible configurations
    around the given setted_values. Then, amoung all possible paths in this graph, it returns the
    first consistent configuration found.
    In this context, a path in the graph described by the adjacency list is a list of param type values
    representing a possible configuration of param types.

    :setted_values: list of param type values that are fixed
    '''

    adj_list = get_adjacency_list(setted_values)

    all_configs = get_all_paths("root", adj_list)

    cons_configs = filter(is_consistent, all_configs)
    [first_config, *_] = cons_configs

    return first_config
