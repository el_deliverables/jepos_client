class Input():
    def __init__(self, user, name, problem, optimiser):
        self.user = user
        self.name = name
        self.problem = problem
        self.optimiser = optimiser


#     def asdict(self):
#         return convert2serialize(self)

    def asdict(self):
        input_dictionary = {}
        input_dictionary['user'] = self.user
        input_dictionary['name'] = self.name
        input_dictionary['problem'] = self.problem.asdict()
        input_dictionary['optimiser'] = self.optimiser.asdict()

        return input_dictionary
