import unittest as ut
from check_solution import *

class TestBMWSSS(ut.TestCase):
    
    def test_solution1(self):
        solution = BMWSSS_solution([10,5,10,5,10,10,0,0,8,5,0,9,7,9,7,7,9,4,9,7],5,20,70,2)
        self.assertEqual(solution.isFeasible(), True, "This is a feasible solution")
    
    def test_solution2(self):
        solution = BMWSSS_solution([10,5,10,5,10,10,0,0,8,5,7,9,7,9,7,7,9,4,9,7],5,20,70,2)
        self.assertEqual(solution.isFeasible(), True, "This is a feasible solution")
    
    def test_solution3(self):
        solution = BMWSSS_solution([10,5,10,5,10,10,0,0,8,5,0,0,7,9,7,7,9,4,9,7],5,20,70,2)
        self.assertEqual(solution.isFeasible(), False, "This is not a feasible solution")
    
    def test_solution4(self):
        solution = BMWSSS_solution([10,5,10,5,10,10,0,0,8,5,7,9,7,9,7,7,9,4,9,0],5,20,70,2)
        self.assertEqual(solution.isFeasible(), False, "This is not a feasible solution")

if __name__ == "__main__":
    ut.main()
