from abc import ABC, abstractmethod
# from .parameters import (JeposParams, CorrelationParams, AcceptanceParams, CostfuncParams, BetaParams, McmcParams,
#                          FastqaoaParams, TrainableParams)
from .parameters.jeposParams import *
from .parameters.fastqaoaParams import FastqaoaParams, TrainableParams
from .parameters.jeposParams import JeposParams
from .parameters.jeposParams import JeposParams
from .problem import PUBOProblem
from .helper_functions import convert2serialize
from typing import Type, Union, List

class Optimiser(ABC):
    """
    Abstract class to represent an optimiser
    """
    def adjust_params(self, problem: PUBOProblem):
        pass

    def __validate_params(self):
        pass

    def asdict(self):
        pass


class SimulatedAnnealing(Optimiser):

    def __init__(self, n_replicas=1, n_iter=100, beta_start=1e-3,
                 beta_factor=1.04, acceptance_type='metropolis_acceptance', mcmc_steps=None,
                 encoding=None):
        self.provider = 'entropica'
        self.algorithm = 'simulated annealing'
        self.n_replicas = n_replicas
        self.n_iter = n_iter
        self.beta_start = beta_start
        self.beta_factor = beta_factor
        self.acceptance_type = acceptance_type
        self.mcmc_steps = mcmc_steps
        self.encoding = encoding

    def __validate_params(self):
        if  type(self.replicas) != int or self.n_replicas <= 0:
            raise ValueError('The number of replicas must be a positive integer')

        if type(self.n_iter) != int or self.n_iter <= 0:
            raise ValueError('The number of iterations must be a positive integer')

        if self.beta_start < 0:
            raise ValueError('The starting inverse temperature must have a positive value')

        if self.beta_factor < 1:
            raise ValueError('The factor when updating the inverse temperature must be greater or equal to one')

        valid_acceptance_types = ['metropolis', 'glauber']

        if self.acceptance_type not in valid_acceptance_types:
            raise ValueError(f'Invalid value for acceptance_type, should be one of {valid_acceptance_types}')

        if type(self.mcmc_steps) != int or self.mcmc_steps <= 0:
            raise ValueError('The number of Monte-Carlo sweeps must be a positive integer')

        if type(self.encoding) != list:
            raise ValueError('The encoding of variables must be given as a list of values')


    def asdict(self):
        # Set jepos parameters following its structure
        costfunc_params = CostfuncParams(encoding=self.encoding)
        beta_params = BetaParams('modlam_beta', self.beta_start, self.beta_factor)
        acceptance_params = AcceptanceParams(self.acceptance_type)
        mcupdate_params = McupdateParams(acceptance_params = acceptance_params)
        mcmc_params = McmcParams(mcmc_steps=self.mcmc_steps, mcupdate_params = mcupdate_params)
        correlation_params = CorrelationParams(correlation_type='SA',
            correlation_steps=self.n_iter,
            beta_params=beta_params)

        # Finally set the Jepos Parameters class
        parameters = JeposParams(nr_replicas=self.n_replicas,
            costfunc_params=costfunc_params,
            correlation_params=correlation_params,
            mcmc_params=mcmc_params)
        return {
                "provider": self.provider,
                "algorithm": self.algorithm,
                "jeposParams" : convert2serialize(parameters)
                }

    def adjust_params(self, problem):
        n_variables = problem.n
        problem_encoding = problem.encoding

        if not self.mcmc_steps:
            self.mcmc_steps = n_variables

        if not self.encoding:
            self.encoding = problem_encoding


class ParallelTempering(Optimiser):

    def __init__(self, n_replicas=1, n_iter=100, beta_start=1e-3,
                 beta_factor=1.04, acceptance_type='metropolis_acceptance', mcmc_steps=50,
                 encoding=None):
        self.provider = 'entropica'
        self.algorithm = 'parallel tempering'
        self.n_replicas = n_replicas
        self.n_iter = n_iter
        self.beta_start = beta_start
        self.beta_factor = beta_factor
        self.acceptance_type = acceptance_type
        self.mcmc_steps = mcmc_steps
        self.encoding = encoding

    def asdict(self):
        # Set jepos parameters following its structure
        costfunc_params = CostfuncParams(encoding=self.encoding)
        beta_params = BetaParams('geometric_beta', self.beta_start, self.beta_factor)
        acceptance_params = AcceptanceParams(self.acceptance_type)
        mcmc_params = McmcParams(mcmc_steps=self.mcmc_steps, acceptance_params=acceptance_params)
        correlation_params = CorrelationParams(correlation_type='PT',
            correlation_steps=self.n_iter,
            beta_params=beta_params,
            acceptance_params=AcceptanceParams())

        # Finally set the Jepos Parameters class
        parameters = JeposParams(nr_replicas=self.n_replicas,
            costfunc_params=costfunc_params,
            correlation_params=correlation_params,
            mcmc_params=mcmc_params)
        return {
                "provider": self.provider,
                "algorithm": self.algorithm,
                "jeposParams" : convert2serialize(parameters)
                }

    def adjust_params(self, problem):
        n_variables = problem.n
        problem_encoding = problem.encoding

        if not self.mcmc_steps:
            self.mcmc_steps = n_variables

        if not self.encoding:
            self.encoding = problem_encoding

class QAOA(Optimiser):

    ALLOWED_INIT_TYPES = ['rand','ramp','custom']
    ALLOWED_PARAM_TYPES = ['standard','extended','fourier'] #can add annealing in future
    SUPPORTED_SCIPY_METHODS = ['cobyla','nelder-mead']
    SUPPORTED_BACKENDS = ['vector','projectq'] 

    def __init__(self, backend='vector', qubit_register = [],
                 p=1, q=1, param_type = 'standard', init_type = 'ramp',
                 trainable_params: TrainableParams = None, method='cobyla', gradient_method='',
                 timesteps=50, maxiter=50, wavefunction=False, optimise=True,
                 api_token=None, hub=None, group=None, project=None, job_monitor=True):
        """
        Currently supports:

        INIT_TYPES = ['rand','ramp','custom']

        PARAM_TYPES = ['standard','extended','fourier']

        SKQUANT_METHODS = ['imfil','bobyqa','snobfit']

        CUSTOM_GRADIENT_METHODS = ['vgd', 'sgd', 'rmsprop']
        
        SCIPY_METHODS = ['nelder-mead','powell','cg','bfgs','newton-cg','l-bfgs-b','tnc','cobyla',
                        'slsqp','trust-constr','dogleg','trust-ncg','trust-exact','trust-krylov']
        """
        
        self.provider = 'entropica'
        self.algorithm = 'fastqaoa'
        self.backend = backend.lower()
        self.qubit_register = qubit_register
        self.p=p
        self.q=q
        self.param_type=param_type.lower()
        self.init_type=init_type.lower()
        self.trainable_params=trainable_params
        self.method=method.lower()
        self.gradient_method=gradient_method.lower()
        self.maxiter=maxiter
        self.timesteps=timesteps
        self.wavefunction=wavefunction
        self.optimise=optimise
        self.api_token=api_token
        self.hub=hub
        self.group=group
        self.project=project
        self.job_monitor=job_monitor        

        ##LD## This seems cumbersome: we should have a simpler way to do it 
        self.fastqaoa_params = FastqaoaParams(self.backend, self.qubit_register, self.p, self.q, self.param_type,
                                              self.init_type, self.trainable_params, self.method, self.gradient_method,
                                              self.timesteps,self.maxiter, self.wavefunction, self.optimise,
                                              self.api_token, self.hub, self.group, self.project,
                                              self.job_monitor )

    def __validate_params(self):
        if type(self.p) != int or self.p <= 0:
            raise ValueError("Number of QAOA layers must be a positive integer")

        if self.backend not in QAOA.SUPPORTED_BACKENDS:
            raise ValueError(f"Please choose a backend from {QAOA.SUPPORTED_BACKENDS}")

        if self.param_type=='fourier' and type(self.q) != int or self.q <= 0:
            raise ValueError("Number of QAOA fourier layers must be a positive integer")

        if self.param_type not in QAOA.ALLOWED_PARAM_TYPES or self.init_type not in QAOA.ALLOWED_INIT_TYPES:
            raise ValueError(f"Parameterisation not supported, please select from {QAOA.ALLOWED_PARAM_TYPES} and {QAOA.ALLOWED_INIT_TYPES}")

        ##LD## update this to support all types
        # if self.method not in QAOA.SUPPORTED_SCIPY_METHODS:
        #     raise ValueError(f"Supported scipy classical optimisers are {QAOA.SUPPORTED_SCIPY_METHODS}")

        if type(self.qubit_register) != List[int]:
            raise ValueError("qubit register can either be left empty or List[Int]")

        if self.init_type == 'custom' and self.trainable_params is None:
            raise ValueError("For custom parameterisation, user must specify intial trainable parameters")
        
        if self.init_type == 'custom' and self.trainable_params is not None:
            if self.param_type == 'standard':
                try:
                    self.trainable_params.betas and self.trainable_params.gammas
                except AttributeError:
                    raise ValueError(f"Incorrect trainable parameters for the choice of {self.param_type} parameterisation")
            
            elif self.param_type == 'extended':
                try:
                    self.trainable_params.betas and self.trainable_params.gammas_singles and self.trainable_params.gammas_pairs
                except AttributeError:
                    raise ValueError(f"Incorrect trainable parameters for the choice of {self.param_type} parameterisation")

            elif self.param_type == 'fourier':
                try:
                    self.trainable_params.u and self.trainable_params.v
                except AttributeError:
                    raise ValueError(f"Incorrect trainable parameters for the choice of {self.param_type} parameterisation")

    def asdict(self):
        #validate specified parameters
        # self.__validate_params()
        return {
                'provider':self.provider,
                'algorithm':self.algorithm,
                'fastqaoaParams':convert2serialize(self.fastqaoa_params)
                }

    def adjust_params(self, problem):
        """
        Initialise the trainable parameters for QAOA according to the specified 
        strategies and by passing the problem statement
        """
        _ = self.fastqaoa_params.initialise_abstract_params(problem)

        return self