from typing import Union
from pydantic import Field, Extra

from ..enums import *
from ..baseModel import BaseModel
from ..validator_tools import *
from .mcupdateParams import McupdateParams, DefaultMcupdateParams

class DefaultMcmcParams(BaseModel):
    """
    A DefaultMcmcParams object specifies the Monte Carlo rule.

    Attributes
    ----------
    mcmc_type: str, constant McmcType.default
        value indicating the default MCMC rule
    mcmc_steps: int
        the number of Monte-Carlo updates performed in a sweep
    mcupdate_params: McupateParams
        the Monte Carlo update rule
    """
    mcmc_type: McmcType = Field(McmcType.default, const=True)
    mcmc_steps: int = 50
    mcupdate_params: McupdateParams = DefaultMcupdateParams()

    _ = check_positive('mcmc_steps')

class RSMcmcParams(BaseModel):
    """
    A RSMcmcParams object specifies a random sampling Monte Carlo rule.

    Note
    ----
    This is used for debugging purposes only

    Attributes
    ----------
    mcmc_type: str, constant McmcType.random_sample
        value indicating the the random sampling MCMC rule
    mcmc_steps: int
        the number of Monte-Carlo updates performed in a sweep
    """
    mcmc_type: McmcType = Field(McmcType.random_sample, const=True)
    mcmc_steps: int = 50

    _ = check_positive('mcmc_steps')

McmcParams = Union[DefaultMcmcParams, RSMcmcParams]