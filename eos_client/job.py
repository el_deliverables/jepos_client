import json
import numpy as np
import requests

from .result import Result

from .inputs.models.input import Input
from .client import Client

class Job:
    COMPLETED = 'completed'
    IN_PROGRESS = 'in progress'
    FAILED = 'failed'
#     JEPOS_APP_URL = "https://entropicacompute.com/"
    JEPOS_APP_URL = "https://bloch.entropicacompute.com/"

    def __init__(self, client=Client(), input_dict={}, job_id=None, backend="debug"):
        self.client = client
        self.input_dict = input_dict
        self.job_id = job_id
        self.status_ = Job.IN_PROGRESS
        self.result = None
        self.backend = backend

    def __repr__(self):
        return repr(self.__dict__)

    def jepos_header(self):
        auth_header = self.client.username+":"+self.client.token
        return {'Authentication': auth_header, 'Content-Type': 'application/json'}

    def submit_to_jepos_app(self):
        response = requests.post(self.JEPOS_APP_URL+"runjepos", headers=self.jepos_header(), json=self.input_dict).json()
        try:
            self.job_id = response['id']
        except:
            print('Job ID cannot be retrieved')
        return response

    def submit_to_eos(self):
        response = requests.post(self.client.urls['url_optimizer'],
                                   json=self.input_dict,
                                   headers=self.client.token).json()

        try:
            self.job_id = response['id']
        except:
            raise Exception('Job ID cannot be retrieved')

        return response

    def submit(self, backend="debug"):
        self.backend = backend
        if backend == "debug":
            print(self.input_dict)
        elif backend == "eos":
            response = self.submit_to_eos()
        elif backend == "eqaoa_app":
            print("to do: access to eqaoa_app")
        elif backend == "jepos_app":
            response = self.submit_to_jepos_app()
        else:
            print("unknown backend: possible backends are \"debug\", \"jepos_app\", \"eqaoa_app\" (to do) and \"eos\" (to do).")

        return response

    def retrieve_from_eos(self):
        if self.status() != Job.COMPLETED:
            raise ValueError('Cannot retrieve the solution as the job is not completed')

        result_response = requests.get(self.client.urls['url_get_job_by_id'],
                                       {'id': self.job_id},
                                       headers=self.client.token)
        res = result_response.json()

        try:
            otimisation_result = res['result'].replace("'", "\"").replace('False', '"False"').replace('True', '"True"')
            otimisation_result = json.loads(otimisation_result)
        except Exception as e:
            otimisation_result = None
            print(f'Exepction {e}')

        r = Result(response=result_response.json(),
                   job_id= self.job_id,
                   solution=res['solution'],
                   time_delta=res['timedelta'],
                   cost=res['cost'],
                   otimisation_result = otimisation_result
                   )

        self.result = r

    def retrieve_from_jepos_app(self):
        response = requests.post(self.JEPOS_APP_URL+"retrieveresults", headers=self.jepos_header(), json={'id': self.job_id}).json()
        if response['status'] == 'in progress':
            print('id: ' + response['id'])
            print('Computation is still in progress. Run this command again to check on the status of the')
            print('computation and retrieve the results once they are available. The time limit for this')
            print('computation has been set to '+str(response["params"]["optimiser"]["jeposParams"]["timeout"])+'s.')
        else:
            print('id: ' + response['id'])
            print('status: ' + response['status'])

        return response

    def retrieve(self):
        if self.backend == "debug":
            print("backend set to \"debug\". Nothing to be retrieved.")
        elif self.backend == "eos":
            response = self.retrieve_from_eos()
        elif self.backend == "eqaoa_app":
            print("to do: retrieve from eqaoa_app")
        elif self.backend == "jepos_app":
            response = self.retrieve_from_jepos_app()
        else:
            print("unknown backend: possible backends are \"debug\", \"jepos_app\", \"eqaoa_app\" (to do) and \"eos\" (to do).")

        return response



    def status(self):
        if self.status_ != Job.COMPLETED:
            response = requests.get(self.client.urls['url_get_job_status'],
                                    {'jobid': self.job_id},
                                    headers=self.client.token)
            json_response = response.json()
            self.status_ = json_response['status']
        return self.status_

def execute(client, problem, optimiser, debug=False):
    print(
        f'Running {optimiser.algorithm} on {optimiser.provider}'
    )

    ## TODO add proper try/catch for submission

    input_dict = create_input_dict(client, problem, optimiser)

    job = Job(client, input_dict)
    # Currently returning a job and not response is a bit inconsistent
    response = job.submit(debug=debug)
    return job

def create_input_dict(problem, optimiser, client=Client()):
    # Adjust the problem-dependent parameters of the optimiser
    optimiser.adjust_params(problem)

    input_dict = {}
    input_dict['user'] = client.username
    input_dict['name'] = 'random_name'
    input_dict['problem'] = problem.asdict()
    input_dict['optimiser'] = optimiser.asdict()

    return input_dict

def create_input(problem, optimiser, client=Client()):
    input = Input(user=client.username, name='random_name', problem=problem, optimiser=optimiser)
    return input
