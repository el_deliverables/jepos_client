from pydantic import Extra, ValidationError
import os
import json

CONFIGS = {'extra': Extra.forbid, 'allow_mutation': False}
JSON_ARG = {'exclude_none': True, "indent":4}
DICT_ARG = {'exclude_none': True}


current_dir = os.path.dirname(__file__) #<-- absolute dir the script is in

ref_tags_path = os.path.join(current_dir, 'jepos_compatibility/tags.json')
with open(ref_tags_path, 'r') as f:
    REF_TAGS = json.loads(f.read()) # the assignement of tags to each possible param type value.

ref_conflicts_vec_path = os.path.join(current_dir, 'jepos_compatibility/conflicts.json')
with open(ref_conflicts_vec_path, 'r') as f:
    REF_CONFLICTS_VEC = json.loads(f.read())['conflicts_vec']

edge_rule_path = os.path.join(current_dir, 'jepos_compatibility/edge_rules.json')
with open(edge_rule_path, 'r') as f:
    EDGE_RULE = json.loads(f.read())

imp_path = os.path.join(current_dir, 'jepos_compatibility/implementations.json')
with open(imp_path, 'r') as f:
    IMP = json.loads(f.read())

imp_path = os.path.join(current_dir, 'jepos_compatibility/types_to_field.json')
with open(imp_path, 'r') as f:
    TYPES_TO_FIELD = json.loads(f.read())
