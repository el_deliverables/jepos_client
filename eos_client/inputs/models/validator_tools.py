'''
This file contains helper fonctions for validation.
'''

from pydantic import validator, root_validator
from pydantic.fields import ModelField

# for validators raising errors, use `field:ModelField` to ensure errors are catch by editor

## Basic checks

def empty_error(v, field:ModelField):
    if v is None:
        return v
    elif len(v) == 0:
        raise ValueError('cannot be empty')

    return v

def zero_error(v, field:ModelField):
    if v is None:
        return v
    elif v == 0:
        raise ValueError('cannot be zero')

    return v

def negative_error(v, field:ModelField):
    if v is None:
        return v
    elif v < 0:
        raise ValueError('cannot be negative')

    return v

def non_positive_error(v, field:ModelField):
    if v is None:
        return v
    elif v <= 0:
        raise ValueError('cannot be non-positive')

    return v

def check_positive(*v, opt={}):
    return validator(*v, **opt, allow_reuse=True)(non_positive_error)

def check_non_empty(*v):
    return validator(*v, allow_reuse=True)(empty_error)

## For OutputParams

def error_if_no_best_or_equilibrium(cls, values):
    best = values.get('best')
    equilibrium = values.get('equilibrium')
    best_or_equ_specified = best is True or equilibrium is True

    if not best_or_equ_specified:
            raise ValueError('specify either best and/or equilibrium results')

    return values

def check_best_or_equilibrium():
    return root_validator(allow_reuse=True, skip_on_failure=True)(error_if_no_best_or_equilibrium)

## For BetaParams

def error_if_lower_than_1(cls, v, values, field:ModelField):
    if v < 1:
        raise ValueError('The factor when updating the inverse temperature must be greater or equal to one')
    return v

def check_at_least_1(*v):
    return validator(*v, allow_reuse=True)(error_if_lower_than_1)


def error_if_leq_than_1(cls, v, values, field:ModelField):
    if v <= 1:
        raise ValueError('The exponent of the cauchy beta schedule must be stricly greater than one')
    return v

def check_str_gt_1(*v):
    return validator(*v, allow_reuse=True)(error_if_leq_than_1)