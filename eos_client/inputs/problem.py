from .helper_functions import convert2serialize
from collections import defaultdict

import numpy as np
import scipy
import scipy.spatial

BINARY_ENCODING = [0, 1]
ISING_ENCODING = [-1, 1]
TERMS_CLEANING_LIMIT = 5000


class PUBOProblem:
    def __init__(self, n, terms=[], weights=[], encoding=ISING_ENCODING, clean_terms_and_weights=False):
        # Check that terms and weights have matching lengths
        if len(terms) != len(weights):
            raise ValueError('The number of terms and number of weights do not match')

        # If the user wants to clean the terms and weights or if the number of
        # terms is not too big, we go through the cleaning process
        if clean_terms_and_weights or len(terms) <= TERMS_CLEANING_LIMIT:
            self.terms, self.weights = PUBOProblem.clean_terms_and_weights(terms, weights)
        else:
            self.terms, self.weights = terms, weights

        self.n = n
        self.encoding = encoding

    def asdict(self):
        return convert2serialize(self)

    @staticmethod
    def clean_terms_and_weights(terms, weights):
        """ Goes through the terms and weights and group them when possible"""
        # List to record the terms as sets
        unique_terms = []

        # Will record the weight for the unique terms (note that since Sets are
        # unhashable in Python, we use a dict with integers for the keys, that
        # are mapped with the corresponding indices of terms from unique_terms)
        new_weights_for_terms = defaultdict(float)

        # We do one pass over terms and weights
        for term, weight in zip(terms, weights):

            # Convert the term to a set
            term_set = set(term)

            # If this term is not yet recorded, we add it to the list of unique
            # terms and we use that it is the last element to find its index
            if term_set not in unique_terms:
                unique_terms.append(term_set)
                term_index = len(unique_terms) - 1

            # Else if the term is alreaddy recorded, we just need to retrieve
            # its index in the unique_terms list
            else:
                term_index = unique_terms.index(term_set)

            # Update the weight in the dictionary using the retrieved index
            new_weights_for_terms[term_index] += weight

        # Return terms and weights, making sure to convert the terms back to lists
        return [list(term) for term in unique_terms], list(new_weights_for_terms.values())

    @staticmethod
    def random_qubo(n, density=0.5, format_m='coo', max_abs_value=100):
        # Generate a random matrix (elements in [0, 1]) of type sparse
        random_matrix = scipy.sparse.rand(n,
                                          n,
                                          density=density,
                                          format=format_m)

        # Retrieve the indices of non-zero elements of the matrix as list of tuples
        terms = np.transpose(random_matrix.nonzero())

        # Get the matrix entries in a list, but scale the elements and
        # make them centered at 0 by subtracting 0.5
        weights = max_abs_value * (random_matrix.data - 0.5)

        # Return the terms and weights, taking care of converting to the correct types
        return PUBOProblem(n, [list(map(int, i)) for i in terms],
                           [float(i) for i in weights],
                           encoding=BINARY_ENCODING)


class TSP:
    def __init__(self, coordinates=None, n_cities=10, seed=0):
        if coordinates:
            self.coordinates = np.array(coordinates)
        else:
            np.random.seed(seed)
            box_size = np.sqrt(n_cities)
            self.coordinates = box_size * np.random.rand(n_cities, 2)

        self.n_cities = self.coordinates.shape[0]

    def get_distance_matrix(self):
        # Return distance matrix: it uses Euclidean distance
        return scipy.spatial.distance_matrix(self.coordinates,
                                             self.coordinates)

    def all_pairs_all_steps(self):
        all_pairs_dict = {}
        for i in range(self.n_cities):
            for j in range(self.n_cities):

                if i != j:
                    all_pairs_dict[(i, j)] = TSP.city_pair_all_steps(
                        i, j, self.n_cities)

        return all_pairs_dict

    @staticmethod
    def city_pair_all_steps(c1, c2, n_cities):

        var_pairs = [(c1 + n_cities * j, c2 + n_cities * (j + 1))
                     for j in range(n_cities)]

        return var_pairs

    def TSP_instance_dict(self, pairs_dict, dists):
        problem_dict = defaultdict(float)

        for city_pair, var_pairs in pairs_dict.items():

            city_dist = dists[city_pair]

            for pair in var_pairs:
                problem_dict[pair] += city_dist

        return problem_dict

    def get_pubo_problem(self):
        distance_matrix = self.get_distance_matrix()

        # Basic problem
        polys_dict = TSP.all_pairs_all_steps(self)
        problem_dict = TSP.TSP_instance_dict(self, polys_dict, distance_matrix)
        pairs = [list(pair) for pair in problem_dict.keys()]
        coeffs = list(problem_dict.values())

        return PUBOProblem(self.n_cities * (self.n_cities + 1),
                           pairs,
                           coeffs,
                           encoding=BINARY_ENCODING)


class NumberPartition:
    def __init__(self, numbers=None):

        # Set the numbers to be partitioned. If not given, generate a random list with integers
        self.numbers = numbers if numbers else list(
            map(int, np.random.randint(1, 10, size=10)))
        self.n_numbers = len(self.numbers)

    def get_pubo_problem(self, encoding=ISING_ENCODING):
        terms = []
        weights = []
        constant_term = 0

        # Consider every pair of numbers (ordered)
        for i in range(self.n_numbers):
            for j in range(i, self.n_numbers):

                # If i equals j, then whatever random sign we choose, if we square
                # it we can back 1. So we have a constant term.
                if i == j:
                    constant_term += self.numbers[i] * self.numbers[j]

                # Otherwise the weight is computed as being the product of the
                # numbers in the pair, multiplied by 2 (since we account for
                # both pair (i, j) and (j, i)
                else:
                    term = [i, j]
                    weight = 2 * self.numbers[i] * self.numbers[j]

                    terms.append(term)
                    weights.append(weight)

        # If the constant term is non-zero, we may add it to terms and weights
        if constant_term > 0:
            terms.append([])
            weights.append(constant_term)

        return PUBOProblem(self.n_numbers,
                           terms,
                           weights,
                           encoding=encoding)
