from enum import Enum, IntEnum
from pydantic import Extra, ValidationError, validator, root_validator

from .enums import *
from .baseModel import BaseModel
from .validator_tools import *
from .params.initParams import InitParams, DefaultInitParams, TSPRandomInitParams
from .params.outputParams import OutputParams, DefaultOutputParams
from .params.mcmcParams import McmcParams, DefaultMcmcParams
from .params.correlationParams import CorrelationParams, DefaultCorrelationParams
from .params.costfuncParams import CostfuncParams, DefaultCostfuncParams, TSPCostfuncParams

class JeposParams(BaseModel):
    """"
    A JeposParams object contains all the parameters related to jepos.

    Attributes
    ----------
    print: bool
        whether to print intermediate results to the terminal —mostly for debugging purposes
    timeout: int
        the time after which the solver terminates and returns the best solution found up until then
    nr_of_replicas: int
        the size of the ensemble
        For Simulated Annealing, this is simply the number of replicas that are annealed in parallel.
        For Parallel Tempering and Population Annealing, the replicas in the ensemble are correlated
        by beta swaps and equilibration respectively.
    costfunc_params: CostfuncParams
        the description of the cost function to minimize
    mcmc_params: McmcParams
        the Markov Chain Monte Carlo parameters
    correlation_params: CorrelationParams
        the parameters of the correlation routine
    init_params: InitParams
        how to initialize the initial ensemble of replicas
    output_params: OutputParams
        what type of output to return
    """
    print: bool = False
    timeout: int = 600
    nr_of_replicas: int = 100 # TODO: how to choose default nr_of_replicas ?
    costfunc_params: CostfuncParams = DefaultCostfuncParams()
    mcmc_params: McmcParams = DefaultMcmcParams()
    correlation_params: CorrelationParams = DefaultCorrelationParams() # has to come after nr_of replicas and mcmc_params
    init_params: InitParams = DefaultInitParams()
    output_params: OutputParams = DefaultOutputParams()

    _ = check_positive('nr_of_replicas', 'timeout')

    @validator('correlation_params')
    def check_two_replicas_for_PT(cls, v, values):
        """
        Verifies, and if need be changes accordingly, that the nr_of_replicas is at least 2 for a PT algorithm.
        """
        cor_type_is_PT = (v.correlation_type == CorrelationType.parallel_tempering)
        nr_of_replicas = values["nr_of_replicas"]

        if cor_type_is_PT and nr_of_replicas < 2:
            values["nr_of_replicas"] = 2
        return v

    @root_validator(skip_on_failure=True)
    def set_acceptance_params_default_for_PT(cls, values):
        """
        For a PT correlation type, if no acceptance_params is specified, use per default the same as
        the acceptance_params in mcmc_params.
        """
        cor_params = values['correlation_params']
        cor_type_is_PT = (cor_params.correlation_type == CorrelationType.parallel_tempering)
        acc_params_not_specified = cor_params.dict(exclude_unset=True).get('acceptance_params') is None

        if cor_type_is_PT and acc_params_not_specified:
            mcmc_params = values['mcmc_params']
            mcmc_acc_params = mcmc_params.mcupdate_params.acceptance_params

            new_cor_params = cor_params.update({'acceptance_params': mcmc_acc_params})
            values['correlation_params'] = new_cor_params

        return values

    @root_validator(skip_on_failure=True)
    def check_costfunc_consistencies(cls, values):
        """
        For a TSP cost type, verifies, and if need be changes accordingly, that the parameters are consistent.

        The consistencies to verify are, in the case of a TSP cost type:
            _ the initilization type should be of TSP_random_init
            _ the mcupdate rule should be of disccrete_city_swap type

        Note
        ----
        This is a 'manual' consistency fix and should be done automatically in the future.
        Changes to the according fields are done only if those where not setted at creation.
        """
        costfunc = values.get('costfunc_params').cost_type
        TSP_type_costfunc = [CostType.discrete_TSP, CostType.random_TSP, CostType.BP, CostType.random_BP]

        if costfunc in TSP_type_costfunc:
            init_params = values.get("init_params")
            init_type_is_not_set = init_params.dict(exclude_unset=True).get('init_type') is None
            if init_type_is_not_set:
                values["init_params"] = TSPRandomInitParams()

            mcmc = values.get('mcmc_params')
            if mcmc.mcmc_type != McmcType.random_sample:
                mcupdate = mcmc.mcupdate_params
                mcupdate_type_is_not_set = mcupdate.dict(exclude_unset=True).get('mcupdate_type') is None
                if mcupdate_type_is_not_set:
                    update_dict = {"mcupdate_params" : {"mcupdate_type": McupdateType.discrete_city_swap}} # TODO: how to choose discrete or binary city swap ?
                    values["mcmc_params"] = mcmc.update(update_dict)


        return values