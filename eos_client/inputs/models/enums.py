from enum import Enum

class Algorithm(str, Enum):
    simulated_annealing = 'simulated annealing'
    parallel_tempering = 'parallel tempering'
    population_annealing = 'population annealing'
    random_sampling = 'random sampling'

class InitType(str, Enum):
    random = "random_init"
    custom = "custom_init"
    tsp_random = "tsp_random_init"

class OutputType(str, Enum):
    no = 'no_out'
    all = 'all_out'
    custom = 'custom_out'

class McmcType(str, Enum):
    default = 'default_mcmc'
    random_sample = 'RS_mcmc'

class McupdateType(str, Enum):
    binary_city_swap = 'binary_city_swap'
    discrete_city_swap = 'discrete_city_swap'
    discrete = 'discrete_mcupdate'
    continuous = 'continuous_mcupdate'

class AcceptanceType(str, Enum):
    metropolis = 'metropolis_acceptance'
    sym_metropolis = 'sym_metropolis_acceptance'
    glauber = 'glauber_acceptance'

# uses same keys as Algorithm
class CorrelationType(str, Enum):
    simulated_annealing = 'SA'
    parallel_tempering = 'PT'
    population_annealing = 'PA'
    random_sampling = 'RS'

class BetaType(str, Enum):
    geometric = 'geometric_beta'
    linear = 'linear_beta'
    modlam = 'modlam_beta'
    cauchy = 'cauchy_beta'

class CostType(str, Enum):
    QUBO = 'qubo'
    PUBO = 'pubo'
    QUCO = 'quco'
    PUCO = 'puco'
    discrete_TSP = 'discrete_tsp'
    NP = 'np'
    random_PUBO = "random_pubo"
    random_PUCO = "random_puco"
    random_TSP = "random_tsp"
    random_NP = "random_np"
    sinusoid_ladder = "sinusoid_ladder"
    BP = "bp"
    random_BP = "random_bp"
    BMW_SSS = "bmw_sss"
    BMW_Gen = "bmw_gm"