from .constants import JSON_ARG, DICT_ARG

from pydantic import BaseModel as PydanticBaseModel
from pydantic.utils import deep_update
from pydantic import Extra

# for documentation purpose only
def override(f):
    return f

## Custom BaseModel to use in this module.
class BaseModel(PydanticBaseModel):
    """
    This class inherits the pydantic BaseModel and specifies custom configurations.

    It overrides the __init__() constructor to update the set __fields_set__.
    It also overrides the dict() and json() methods to be called with specific arguments.
    """
    class Config:
        extra = Extra.forbid
        allow_mutation = False
        validate_all = True
        validate_assignment = True # AD: think is useless bc allow_mutation=False

    @override
    def __init__(self, **data):
        super().__init__(**data)

        # [In the following, replace <_params> by BetaParams and <_type> by beta_type for example.]
        # Since all fields of type <_params> are broke down into different classes, each with
        # different constant value for the field <_type>, we need to consider the field <_type>
        # as setted, even if it was infered by the choice of the <_param> class. Otherwise, when
        # exporting the object with exclude_unset=True, the choice of <_param> class will be lost.

        type_fields = [f for f in self.__fields__.keys() if "_type" in f]
        self.__fields_set__.update(type_fields)

    @override
    def json(self, **kw):
        kw.update(JSON_ARG)
        return super().json(**kw)

    @override
    def dict(self, **kw):
        kw.update(DICT_ARG)
        return super().dict(**kw)

    def update(self, update_dict):
        """
        Constructs an updated copy of this object.

        Parameters
        ----------
        update_dict, dict
            the dictionarry containing the updated value of the fields
            the keys are the fields, the values the field values
        """
        new_dict = deep_update(self.dict(exclude_unset=True), update_dict)
        return self.__class__(**new_dict)