'''
This file contains functions to check the consistency of an input configuration.


For clarity, here is a quick definition of the vocabulary used.

param type
    the keys of the dict IMP, e.g. 'algorithm', 'mcupdate', 'acceptance2'
param type value
    the value of a param type, an element of the list value in IMP, e.g. "RS', 'continuous_mcupdate', 'metropolis_acceptance2'
config_param_type
    a list of param type values corresponding to a configuration

tag
    a tag mapped to param type values, elements of the list value in REF_TAGS, e.g 'discrete', 'continuous'
tags_vec
    a list of tag, also called tag vector
config_tags_vec
    a list of tag vectors corresponding to a congiguration

conflict_vec
    a list of tags that are in conflict with each other

configuration
    an input instance state

'''

from itertools import combinations, permutations
from .enums import *
from .constants import *
from .baseModel import BaseModel

## Check compatibility

def get_config_param_types(configuration, in_list_values):
    """
    Recursively constructs the list of all param type values in configuration.

    Parameters
    ----------
    configuration: dict
        a dict containing the field_name-to-fiel_value mapping of an input instance

    in_list_values: list
        the incoming list of values

    Returns
    -------
    list
        the list of param type value corresponding to the given configuration
    """
    list_values = in_list_values.copy()

    for key in configuration:
        field_values = configuration[key]

        if isinstance(field_values, BaseModel):
            field_values = field_values.dict()

        if "_type" in key:
            list_values.append(field_values)
        elif type(field_values) == dict:
            list_values = get_config_param_types(field_values, list_values)

    return list_values

def get_config_tags_vec(config_param_types):
    """
    Constructs a list of tag vectors to which is mapped the given list of param type values.

    Parameters
    ----------
    config_param_types: list
        list of param type values

    Returns
    -------
    list of list
        the list of tag vectors to which is mapped the given list of param type values
    """
    config_tags_vec = []
    for param_type_value in config_param_types:
        if param_type_value in REF_TAGS:
            config_tags_vec.append(REF_TAGS[param_type_value])
        else:
            config_tags_vec.append([]) # no tag
    return config_tags_vec

def are_overlapping(tags_vec1, tags_vec2, conflict_vec):
    '''
    Whether there is an overlap between the three given list or tags.

    Parameters
    ----------
    tags_vec1: list
        a list of tags
    tags_vec2: list
        a list of tags
    conflict_vec: list
        a list of tags in conflict with each other

    Returns
    -------
    bool
        whether there is an overlapping between the three given list or tags.
    '''
    for tag in tags_vec1:
        if (tag in tags_vec2) and (tag in conflict_vec):
            return True
    return False

def are_conflicted(tags_vec1, tags_vec2, conflicted_tag_pair, conflict_vec):
    '''
    Whether the tag vectors tags_vec1 and tags_vec2 are in conflict according to the given conflict rules.

    The tag vectors are in conflict if the conflicted_tag_pair appears between the two tag vectors
    and if the tag vectors are not overlapping with the conflict_vec. If they are overlapping, then there is
    a possible tag assignement that doesn't raise a conflict.

    Parameters
    ----------
    tags_vec1: list
        a list of tags
    tags_vec2: list
        a list of tags
    conflicted_tag_pair: tuple
        a pair of tags in conflict
    conflict_vec: list
        a list of tags in conflict with each other

    Returns
    -------
    bool
        whether the tag vectors tags_vec1 and tags_vec2 are in conflict
    '''
    c1_in_t1 = conflicted_tag_pair[0] in tags_vec1
    c2_in_t2 = conflicted_tag_pair[1] in tags_vec2

    conflict = (c1_in_t1 and c2_in_t2)
    overlapping = are_overlapping(tags_vec1, tags_vec2, conflict_vec)
    # if (conflict):
    #     print(tags_vec1, tags_vec2, conflicted_tag_pair, conflict_vec)
    #     print(conflict, overlapping)
    return conflict and not overlapping

def get_incompatible_tags(conflict_vec, config_tags_vec, config_param_types):
    '''
    Constructs a list of inconsistencies found in config_tags_vec with respect to the conflict_vec rule.

    An inconsistency is a tuple (conflicted_value_pair, conflicted_tag_pair) where the conflicted_value_pair
    is the pair of param type values that are mapped to the conflicted_tag_pair and are in conflict with each other.

    Parameters
    ----------
    conflict_vec: list
        a list of tags in conflict with each other
    config_tags_vec: list
        a list of tag vectors
    config_param_types: list
        a list of param type values

    Returns
    -------
    list
        the list of found inconsistencies
    '''
    incompatible_tags = []
    for conflicted_tag_pair in combinations(conflict_vec, 2):
        for tags_ind1, tags_ind2 in permutations(range(len(config_tags_vec)), 2):
            tags_vec1, tags_vec2 = config_tags_vec[tags_ind1], config_tags_vec[tags_ind2]

            if are_conflicted(tags_vec1, tags_vec2, conflicted_tag_pair, conflict_vec):
                conflicted_value_pair  = (config_param_types[tags_ind1], config_param_types[tags_ind2])
                inconsistency = (conflicted_value_pair, conflicted_tag_pair)
                incompatible_tags.append(inconsistency)

    return incompatible_tags

def get_inconsistencies(config_param_types):
    '''
    Search for inconsistencies in config_param_type.

    Parameters
    ----------
    config_param_types: list
        a list of param type values

    Returns
    -------
    tuple(list, str)
        the list of found inconsistencies and the corresponding conflict message
    '''
    # get set of tags
    config_tags_vec = get_config_tags_vec(config_param_types)

    # print("config_param_types", config_param_types)
    # print('config_tags_vec', config_tags_vec)

    conflict_message = ""
    inconsistencies = []

    for conflict_vec in REF_CONFLICTS_VEC: # for each set of conflicted tags
        incompatible_tags = get_incompatible_tags(conflict_vec, config_tags_vec, config_param_types) # finds conflicted tags

        if len(incompatible_tags): # conflict was found
            for consistency in incompatible_tags:
                inconsistencies.append(consistency)

                # write conflict info in message
                conflicted_value_pair, conflicted_tag_pair = consistency
                conflict_message = f"{conflict_message}Conflict in input logic between '{conflicted_value_pair[0]}' and '{conflicted_value_pair[1]}'. "
                conflict_message = f"{conflict_message}They were tagged with '{conflicted_tag_pair[0]}' and '{conflicted_tag_pair[1]}' respectively.\n"

    return inconsistencies, conflict_message
