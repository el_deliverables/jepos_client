### Basic functions necessary to run the shift scheduling problems studied in the project. Presented as a class for convenience

### The structure is general, so one can pass in parameters corresponding to different models considered in the report (Super Super Simple, etc) as required, eg:
# SSSM: n_shops = [1,1], n_shop_types = 2
# SM: n_shops = [1,1,1], n_shop_types = 3
# AFAM: n_shops = [1,1,1], n_shop_types = 3
# AF: n_shops = [2,1,1], n_shop_types = 3

### Limitations
# Penalty schemes 1, 2, and 3 from the report are hard-coded here. Alternatives would need to be written as required
# Supports a maximum of 3 shop types (body, paint, assembly)

### Key functions
# The Monte Carlo moves are performed by the 'shift_switches' method
# The overall cost function is the 'total_cost' method
# Other methods compute various aspects of the solution (e.g. output volume, )

import numpy as np

class BMWGen_solution():

    def __init__(self, solution, init_buffer, max_buffer, target_volume, tolerance, n_shop_types, n_shops, n_shifts, units_per_hour, calendar, block_len, shop_shifts, shop_costs, shift_change_costs, penalty_scheme):
        self.n_days = len(solution)//n_shifts
        self.solution = np.array(solution)
        self.init_buffer = init_buffer
        self.max_buffer = max_buffer
        self.target_volume = target_volume
        self.tolerance = tolerance
        self.n_shop_types = n_shop_types
        self.n_shops = n_shops
        self.n_shifts = n_shifts
        self.units_per_hour = units_per_hour
        self.calendar = calendar
        self.block_len = block_len
        self.shop_shifts = shop_shifts
        self.model_ratios = [i/sum(self.target_volume) for i in self.target_volume]
        self.shop_costs = shop_costs
        self.shift_change_costs = shift_change_costs
        self.penalty_scheme = penalty_scheme
        
    def generate_random_configuration(self):
        n_blocks = self.n_days//self.block_len
        assignment = []
        for i in range(n_blocks):

            block_assignment = []
            for j in range(self.n_shifts):
                shift_assignment = []
                for k in range(sum(self.n_shops)):
                    rand_assignment = float(np.random.choice(self.shop_shifts[k][j]))
                    shift_assignment.append(rand_assignment)
                block_assignment.append(shift_assignment)
            assignment += block_assignment*self.block_len

        return assignment

    def daily_buffer_amount(self):
        buffer_list = np.zeros((self.n_days*self.n_shifts + 1, self.n_shops[0]*(self.n_shop_types - 1)))
        buffer_list[0] = self.init_buffer
        solution_cal = np.multiply(self.solution, self.calendar)
        # Handles maximum of two buffers. We have two cases: one buffer and two buffers.
        if self.n_shop_types == 3:
            for d in range(self.n_days*self.n_shifts):
                # Loops over shifts
                for j in range((self.n_shops[0])):
                    # Loops over sub-buffers
                    # First buffer computations
                    buffer_list[d+1, j] = max(0, buffer_list[d,j] + solution_cal[d,j]*self.units_per_hour[j] - self.model_ratios[j]*solution_cal[d,-2]*self.units_per_hour[-2])
                    # Second buffer computations
                    if buffer_list[d+1, j] == 0:
                        buffer_list[d+1, j+self.n_shops[0]] = max(0, buffer_list[d,j+self.n_shops[0]] + buffer_list[d,j] + solution_cal[d,j]*self.units_per_hour[j] - self.model_ratios[j]*solution_cal[d,-1]*self.units_per_hour[-1])
                    else:
                        buffer_list[d+1, j+self.n_shops[0]] = max(0, buffer_list[d,j+self.n_shops[0]] + self.model_ratios[j]*solution_cal[d,-2]*self.units_per_hour[-2] - self.model_ratios[j]*solution_cal[d,-1]*self.units_per_hour[-1])
        else:
            for d in range(self.n_days*self.n_shifts):
                # Loops over shifts
                for j in range(self.n_shops[0]):
                    # Loops over sub-buffers
                    buffer_list[d+1, j] = max(0, buffer_list[d,j] + solution_cal[d,j]*self.units_per_hour[j] - self.model_ratios[j]*solution_cal[d,-1]*self.units_per_hour[-1])

        return buffer_list

    def cumulative_volume(self):
        volume = np.zeros((self.n_days*self.n_shifts+1, self.n_shops[0]))
        solution_cal = np.multiply(self.solution, self.calendar)
        buffer_list = self.daily_buffer_amount()
        if self.n_shop_types == 3:
            for d in range(1,self.n_days*self.n_shifts+1):
                for j in range(self.n_shops[0]):
                    volume[d,j] = volume[d-1,j] + solution_cal[d-1,j]*self.units_per_hour[j] + buffer_list[d-1,j] + buffer_list[d-1,j+self.n_shops[0]] - buffer_list[d,j] - buffer_list[d,j+self.n_shops[0]]
        else:
            for d in range(1,self.n_days*self.n_shifts+1):
                for j in range(0,self.n_shops[0]):
                    volume[d,j] = volume[d-1,j] + solution_cal[d-1,j]*self.units_per_hour[j] + buffer_list[d-1,j] - buffer_list[d,j]
        return volume
    
    def isFeasible(self):
        """
        Determines the feasibility of the configuration by calculating the penalty cost. For feasible solutions, the penalty cost is 0.
        """ 
        if (self.penalty_cost() == 0.0):
            return True
        else: 
            return False
    
    def shift_switches(self):

        """
        Assumes configs are passed in as an array in the following way

        [[shift_shop1_day1, shift_shop2_day1, shift_shop3_day1],
        ...
        [shift_shop1_dayN, shift_shop2_dayN, shift_shop3_dayN]]

        i.e. the [i,j] element of the array is the number of hours worked by shop j on day i
        """

        new_config = self.generate_random_configuration()

        # Pick the shop, the block to change, and the new shift
        rand_shop = np.random.randint(sum(self.n_shops))
        rand_shift = np.random.randint(self.n_shifts)
        block_to_switch = np.random.randint(self.n_days//self.block_len)

        block_start_day = block_to_switch*self.block_len*self.n_shifts

        current_shift = self.solution[block_start_day + rand_shift][rand_shop]
        other_shifts = [shift for shift in self.allowed_shifts[rand_shift][rand_shop] if shift != current_shift]
        new_shift = np.random.choice(other_shifts)
        
        # Update for every day in the block
        for i in range(self.block_len):
            new_config[block_start_day + self.n_shifts*i + rand_shift, rand_shop] = new_shift

        return new_config
    
    def shift_switching_cost(self):
        """
        Calculates the cost incurred due to the switching of shift from one block to another in a given configuration.
        """
        n_blocks = self.n_days//self.block_len
        shift_bef_inds = [j for k in range(1, n_blocks) for j in range(k*self.block_len*self.n_shifts - self.n_shifts, k*self.block_len*self.n_shifts)] 
        shift_aft_inds = [j for k in range(1, n_blocks) for j in range(k*self.block_len*self.n_shifts, k*self.block_len*self.n_shifts + self.n_shifts)] 
        
        conf_arr = np.array(self.solution)
        shifts_bef = conf_arr[shift_bef_inds] # Shifts from the "current" 4-week block
        shifts_aft = conf_arr[shift_aft_inds] # Shifts from the next 4-week block
        shift_model_changes = np.subtract(shifts_bef, shifts_aft)
        n_changes = np.count_nonzero(shift_model_changes, axis=0)
        
        return np.dot(self.shift_change_costs, n_changes)
    
    def penalty_coeff(self):
        """
        Computes a rough lower bound for the most expensive feasible solution. This is one where we produce exactly the upper volume bound V_target + delta. 
        Assume all shops work the required number of hours to output this amount. We then multiply the total cost of this 'solution' by 5, to get a penalty energy.
        """
        pen = 0
        for i in range(len(self.shop_costs)):    
            pen += (sum(self.target_volume)+sum(self.tolerance))*(self.shop_costs[i]/self.units_per_hour[i])

        return 10*pen
    
    def penalty_cost(self):
        """
        Calculates the cost incurred due to buffer and volume violations given a specific penalty scheme.
        Penalty 1: volume overflow or underflow + buffer violations
        Penalty 2: volume overflow or underflow + sum of all buffer overflow
        Penalty 3: volume overflow or underflow + sum of all buffer overflow/total number of shifts
        """
        cost = 0
        volume = self.cumulative_volume()
        buffer = self.daily_buffer_amount()
        full_buffer = np.zeros((self.n_days*self.n_shifts + 1, self.n_shop_types - 1))
        for i in range(self.n_shop_types - 1):
            full_buffer[:,i] = np.sum(buffer[:,i*self.n_shops[0]:(i+1)*self.n_shops[0]], axis=1)
        P = self.penalty_coeff()
        
        # Volume
        volume_diff = abs(volume[-1,:] - self.target_volume)
        cost += sum([P*max(0,volume_diff[i]-self.tolerance[i]) for i in range(len(self.tolerance))])
        
        # Buffer
        if self.penalty_scheme == 1:
            for i in range(self.n_shop_types - 1):
                cost += np.sum(np.where(full_buffer[:,i] > self.max_buffer[i], P, 0))
        elif self.penalty_scheme == 2:
            for i in range(self.n_shop_types - 1):
                cost += np.sum(np.where(full_buffer[:,i] > self.max_buffer[i], P*(full_buffer[:,i] - self.max_buffer[i])/(self.n_days*self.n_shifts), 0))
        else:
            for i in range(self.n_shop_types - 1):
                cost += np.sum(np.where(full_buffer[:,i] > self.max_buffer[i], P*(full_buffer[:,i] - self.max_buffer[i]), 0))
        
        return cost
           
    def total_cost(self):
        """
        Calculates the total cost of the current configuration: cost of factory operation + cost due to penalty + cost of shift switches.
        """
        # Cost of labour
        working_shifts = np.multiply(self.solution, self.calendar) 
        tot_shop_hours = np.sum(working_shifts, axis=0)
        #cost += np.dot(tot_shop_hours, shop_costs)
        cost = np.sum([tot_shop_hours[j]*self.shop_costs[j] for j in range(len(self.shop_costs))])
        
        # Cost of shift changes
        cost += self.shift_switching_cost()
        
        # Cost of penalty
        cost += self.penalty_cost()
        
        return cost
        
    def init_temp(self):
        """
        This function calculates the initial temperature to be set by randomly sampling moves and taking the average of their energy differences. 
        """
        all_cost = []
        for _ in range(50):
            random_config = self.generate_random_configuration()
            daily_output = random_config*np.multply(self.shop_costs, self.calendar)
            all_cost.append(self.total_cost())
            random_config = self.shift_switches()

        diff = []
        for i in range(49):
            diff.append(abs(all_cost[i] - all_cost[i+1]))

        return max(diff)
