from typing import Union
from pydantic import Field

from ..enums import *
from ..validator_tools import *
from ..baseModel import BaseModel

class GeometricBetaParams(BaseModel):
    """
    A GeometricBetaParams object specifies a geometric beta schedule.

    At iteration k, the inverse temperature is computed as follows
    .. math::
        beta_start * (beta_factor ^ k)

    Attributes
    ----------
    beta_type: str, constant BetaType.geometric
        value indicating a geometric beta schedule
    beta_start: float
        the starting beta
    beta_factor: float
        the factor by which beta is multiplied during each adjustment of the inverse temperature
    """
    beta_type: BetaType = Field(BetaType.geometric, const=True)
    beta_start: float = 1e-6
    beta_factor: float = 1.1

    _ = check_positive('beta_start')
    _ = check_at_least_1('beta_factor')

class LinearBetaParams(BaseModel):
    """
    A LinearBetaParams object specifies a linear beta schedule.

    At iteration k, the inverse temperature is computed as follows
    .. math::
        beta_start + beta_add * in_iter

    Attributes
    ----------
    beta_type: str, constant BetaType.linear
        value indicating a linear beta schedule
    beta_start: float
        the starting beta
    add: float
        the term added to beta during each adjustment of the inverse temperature.
    """
    beta_type: BetaType = Field(BetaType.linear, const=True)
    beta_start: float = 1e-6
    beta_add: float = 1e-6

    _ = check_positive('beta_start')
    _ = check_positive('beta_add')

class ModlamBetaParams(BaseModel):
    """
    A ModlamBetaParams object specifies a modlam beta schedule.
    This is a dynamic schedule, that adjusts the inverse temperature, beta,
    according to a pre-defined acceptance rate schedule for the Monte-Carlo moves.

    Attributes
    ----------
    beta_type: str, constant BetaType.modlam
        value indicating a modlam beta schedule
    beta_start: float
        the starting beta
    beta_factor: float
        the factor by which beta is multiplied during each adjustment of the inverse temperature
    """
    beta_type: BetaType = Field(BetaType.modlam, const=True)
    beta_start: float = 1e-6
    beta_factor: float = 1.1

    _ = check_positive('beta_start')
    _ = check_at_least_1('beta_factor')


class CauchyBetaParams(BaseModel):
    """
    A CauchyBetaParams object specifies a cauchy cooling schedule.

    At iteration k, the inverse temperature is computed as follow,
    where beta_start is the inverse temperature at the 1st iteration:
    .. math::
        beta_start * \sum_{i=1}^{k} 1/(i^beta_exp)


    Attributes
    ----------
    beta_type: str, constant BetaType.cauchy
        value indicating a cauchy beta schedule
    beta_start: float
        the starting beta
    beta_exp: float
        the exponent of the Rieman serie by which beta_start is multiplied
        during each adjustment of the inverse temperature
    """
    beta_type: BetaType = Field(BetaType.cauchy, const=True)
    beta_start: float = 1e-6
    beta_exp: float = 1.1

    _ = check_positive('beta_start')
    _ = check_at_least_1('beta_exp')


BetaParams = Union[GeometricBetaParams, LinearBetaParams, ModlamBetaParams, CauchyBetaParams]
DefaultBetaParams = GeometricBetaParams