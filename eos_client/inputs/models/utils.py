import copy

def replace_value(in_dict, old_value, new_value):
    """
    Recursively replace all null values in the given dict by the given value.

    Parameters
    ----------
    in_dict: dict
        the dict in which to replace the null values
    new_value: Any
        the value replacing all null values in in_dict

    Returns
    -------
    dict
        a copy of the in_dict in which all null values are replaced by the given new_value
    """
    in_dict = copy.deepcopy(in_dict)
    if in_dict == old_value :
        return new_value

    if type(in_dict) == dict:
        return {k: replace_value(v, old_value, new_value) for k, v in in_dict.items()}

    return in_dict