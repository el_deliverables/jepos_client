import abc
from typing import List, Union
from pydantic import Field

from ..enums import *
from ..baseModel import BaseModel
from ..validator_tools import *

class NoOutputParams(BaseModel):
    """
    A NotOutputParams object specifies that no intermediate results should be returned.

    Attributes
    ----------
    output_type: str, constant OutputType.no
        value indicating no intermediate results should be returned
    """
    output_type: OutputType = Field(OutputType.no, const=True)

class AllOutputParams(BaseModel):
    """
    An AllOutputParams object specifies that all intermediate results should be returned.
    More specifically, the results of the best and/or at equilibrium at each temperature step
    can be returned.

    Attributes
    ----------
    output_type: str, constant OutputType.all
        value indicating that all intermediate results should be returned
    best: bool
        whether to return the best results at each temperature step
    equilibrium: bool
        whether to return the equilibrium results at each temperature step
    """
    output_type: OutputType = Field(OutputType.all, const=True)
    best: bool = True
    equilibrium: bool = True

    _ = check_best_or_equilibrium()

class CustomOutputParams(BaseModel):
    """
    A CustomOutputParams object specifies the custom intermediate results that should be returned.

    Records the replicas specified in "replicas" for the temperature steps specified in "steps".

    Attributes
    ----------
    output_type: str, constant OutputType.custom
        value indicating that some intermediate results should be returned
    steps: list of int
        the list of temperature steps for which to record results
    replicas: list of int
        the list of replicas for which to record results
    best: bool
        whether to return the best results in the specified replicas and specified temperature steps
    equilibrium: bool
        whether to return the equilibrium results in the specified replicas and specified temperature steps
    """
    output_type: OutputType = Field(OutputType.custom, const=True)
    steps: List[int]
    replicas: List[int]
    best: bool = True
    equilibrium: bool = True

    _ = check_non_empty('steps', 'replicas')

    _ = check_positive('steps', 'replicas', opt={'each_item':True})

    _ = check_best_or_equilibrium()

OutputParams = Union[NoOutputParams, AllOutputParams, CustomOutputParams]
DefaultOutputParams = NoOutputParams