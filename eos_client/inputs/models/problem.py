from typing import List
from collections import defaultdict
from pydantic import validator

from .enums import *
from .baseModel import BaseModel
from .validator_tools import *

class Problem(BaseModel):
    """
    A Problem object describs a problem's hamiltonian.

    Attributes
    ----------
    n: int
        the number of variables
    term: list of list of int
        the list of multiplicative terms
    weights: list of float
        the respective weights of the multiplicative terms

    Example
    -------
    A Problem object with n=3, terms=[[0, 1], [0, 1, 2]] and weights=[27, 42] describes the hamiltonian
        H = (27 * x_0 * x_1) + (42 * x_0 * x_1 * x_2)
    """
    n: int = 10
    terms: List[List[int]] = [[0,1]]
    weights: List[float] = [16]

    _ = check_positive('n')

    @validator('weights')
    def check_same_len(cls, weights, values):
        """
        Verifies that the number of terms is the same as the number of weights.

        Raises
        ------
        ValueError
            If the number of terms is different from the number of weights.
        """
        terms = values.get('terms')
        if len(terms) != len(weights):
            return ValueError('the number of terms and weights do not match')
        return weights

    # From PUBOProblem
    @staticmethod
    def clean_terms_and_weights(terms, weights):
        """ Goes through the terms and weights and group them when possible"""
        # List to record the terms as sets
        unique_terms = []

        # Will record the weight for the unique terms (note that since Sets are
        # unhashable in Python, we use a dict with integers for the keys, that
        # are mapped with the corresponding indices of terms from unique_terms)
        new_weights_for_terms = defaultdict(float)

        # We do one pass over terms and weights
        for term, weight in zip(terms, weights):

            # Convert the term to a set
            term_set = set(term)

            # If this term is not yet recorded, we add it to the list of unique
            # terms and we use that it is the last element to find its index
            if term_set not in unique_terms:
                unique_terms.append(term_set)
                term_index = len(unique_terms) - 1

            # Else if the term is alreaddy recorded, we just need to retrieve
            # its index in the unique_terms list
            else:
                term_index = unique_terms.index(term_set)

            # Update the weight in the dictionary using the retrieved index
            new_weights_for_terms[term_index] += weight

        # Return terms and weights, making sure to convert the terms back to lists
        return [list(term) for term in unique_terms], list(new_weights_for_terms.values())
