from pydantic import StrictStr, root_validator
from pydantic.utils import deep_update

from .enums import *
from .constants import *
from .baseModel import BaseModel
from .validator_tools import *
from .jeposParams import JeposParams
from .utils import replace_value

class Optimiser(BaseModel):
    """
    An Optimiser object describes an optimisation algorithmm and it's parameters.

    Attributes
    ----------
    provider: str
        the name of the solver's provider
    algorithm: str, element of the enum Algorithm
        the name of the algorithm to use
    jeposParams: JeposParams
        the parameters for the jepos solver
    """
    provider: StrictStr = 'entropica'
    algorithm: Algorithm = Algorithm.simulated_annealing
    jeposParams: JeposParams = JeposParams() # TODO: ask Ulrike, camel case inconsistency

    ## Validations

    _ = check_non_empty('provider')

    @root_validator(skip_on_failure=True)
    def check_algo_consistencies(cls, values):
        """
        Verifies, and if need be changes accordingly, that the jeposParams is consistent with the algorithm.

        The consistencies to verify are:
            _ the correlation type of the jeposParams should be the same as the algorithm
            _ in case of a random sampling algorithm, the mcmc parameter in jeposParams should be of type RS_mcmc

        Note
        ----
        Those are 'manual' consistency fixes and should be done automatically in the future.
        Changes to the according jeposParams fields are done only if those where not setted at creation.
        """
        algo = values.get('algorithm')
        algo_name = Algorithm(algo).name
        jParams = values.get("jeposParams")

        update_dict = {}

        ## consistency with correlation type
        # TODO ask Ulrike, I don't see this check in jepos
        cons_cor_type = CorrelationType[algo_name] # the correlation type consistent with the algorithm

        setted_cor_params = jParams.dict(exclude_unset=True).get('correlation_params')
        cor_type_is_not_set = setted_cor_params is None or setted_cor_params.get('correlation_type') is None

        if cor_type_is_not_set:
            update_dict['correlation_params'] = {"correlation_type": cons_cor_type}
        else: # in case the correlation param was set, update the current algorithm accordingly
            correlation_name = setted_cor_params.get('correlation_type').name
            values['algorithm'] = Algorithm[correlation_name]
        # TODO: should raise a consistency error if algorithm and cor type are setted but not to the same value
        # here, the setted cor type is stronger than setted algorithm


        ## consistency with mcmc type
        setted_mcmc_params = jParams.dict(exclude_unset=True).get('mcmc_params')
        mcmc_type_is_not_set = setted_mcmc_params is None or setted_mcmc_params.get('mcmc_type') is None
        if algo == Algorithm.random_sampling and mcmc_type_is_not_set:
            update_dict["mcmc_params"] = {"mcmc_type": McmcType.random_sample}


        ## update
        cons_jParams = jParams.update(update_dict)
        values['jeposParams'] = cons_jParams

        return values


    @staticmethod
    def get_optimiser_as_dict(options: dict):
        """
        Static method to construct a dictionnary serializable into an Optimiser object from a param type to param type value dictionnary.

        Parameters
        ----------
        options: dict
            the dict of param type to param type values.

        Returns
        -------
        dict
            the dict serializable into an Optimiser object
        """
        jeposParams = {}
        for param_type, param_type_value in options.items():
            param_type_field = TYPES_TO_FIELD[param_type]

            new_jeposParams = replace_value(param_type_field, None, param_type_value)

            jeposParams = deep_update(jeposParams, new_jeposParams)

        if 'algorithms' in options:
            # special update for cor_type and algo bc no infering in this direction
            cor_type = options['algorithms']
            algo = Algorithm[CorrelationType(cor_type).name]

            return {'algorithm':algo, 'jeposParams': jeposParams}

        return {'jeposParams': jeposParams}