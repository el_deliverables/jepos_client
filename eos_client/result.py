import numpy as np
from .inputs.helper_functions import convert2serialize

class Result():
    def __init__(self, response, job_id, solution, time_delta, cost, otimisation_result):
        self.response = response
        self.job_id = job_id
        self.solution = np.fromstring(solution[1:-1], dtype=np.int, sep=',')
        self.time_delta = time_delta
        self.cost = cost
        self.optimiser_version = None ##LD## Must be changed
        self.otimisation_result = otimisation_result

    def __repr__(self):
        return repr(self.__dict__)

    def asdict(self):
        return convert2serialize(self)

    def get_full_solution(self):
        return self.response.json()
